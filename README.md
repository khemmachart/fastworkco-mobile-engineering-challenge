# fastworkco-mobile-engineering-challenge

This project is to use to apply an iOS developer job application for Fastwork engineering team. I spent 10 days  development since 11 Jan 2019 until 24 Jan 2019 (extended 4 days as I have unexpected situation) according to my challenge from Fastwork. Thank you Fastwork to challenge me, it was so excited.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- Xcode 10.1 or any version which is support Swift 4.2
- Cocoapods as dependency manager
- Device or simulator which has iOS 9.0 installed as a minimum iOS version

### Installing

- Clone the project
- Open terminal, go to your project directory and run 'pod install'
- Open xcworkspace

## Running the tests

Unit testing and integration testing are included in this project. You can run test cases by Product > Test.
This project is behavior-driven testing, by using Quick and Nimble as a framework.

## Built With

* Juanpe Catal�n - VIPER template generator
* apploft (GmbH) - Expandable label

## Versioning

You can checkout Master for a stable version or development for unstable version

## Authors

- Khemmachart Chutapetch
- Medium: https://medium.com/@khemmachart
- Github: https://github.com/khemmachart


# Challenge problem and solution

### Description of the problem and solution.
- Limited time for development: I need to estimate and allocated my free time to achieve my challenge within 10 days. The most important thing is that, I have tried to use new technologies (new framework) that I have no experience, such as, Codable protocol, URLSession. So I need to allow my time for unknown issues. I will write what I have learn from this project at the end of this document.
- No resources proviede such as image. I decided to use irrelevant images to have more time for development.

### Whether the solution focuses on back-end, front-end or if it's full stack:
- I give the code higher priority than visualization, use irrelevant images, and pay less attention for color. I try to keep the layout and flow (including UX) correct according to challenge

### Reasoning behind your technical choices, including architectural.
- I'm using VIPER as software architecture. With VIPER, I can adapt SOLID priciple to this project. VIPER is make project look like a jigsaw, we can plug in any element so easliy. For example, if you prefer to use waterfall collection view (like Pinterest) for product list when display on tablet screen, just plug it with 3 line of codes. Take time for first development but easy to maintain.

### Trade-offs you might have made, anything you left out, or what you might do differently if you were to spend additional time on the project.
- Project environments should be Debug, Release, and QA
- Project resource should be different on each target, such as, application bundle ID, application name, endpoints, application icon. etc.
- Class's name and variables's name should be more meaningful
- Should have more configuration manager. For example. The loggingManager, it should be able to config logging level. Now I just hardcode to display service response only on development environment.


### Project Conclusion
- Architecture - VIPER
- Environments - Debug, Release
- Testing frameworks - Quick, Numble
- Loging: Only ServiceManager has a response log and only display on debug environment

## Acknowledgments

Thank you Fastwork to challenge me. I gain a lot of knowledge from develop this project. Here are what I have learned so far from develop this project.

- Swift 4.2: I used to work with Swift 4.0. This project I decided to use 4.2, saw some syntaxes are changed. Such as, notification name, etc.
- URLSession for Service manager. I alway use Alamofire for all iOS projects I have done, including currently project I'm doing as a networking manager. As a recommendation from senior iOS developer from Agoda. Using URLSession and Result type is good enough for networking. Use Alamofire only when we need advance features.
- Codable protocol for mapping object. This protocol comes with Swift 4. I am familiar to map an object manually or using JSON object mapper because almost of the various projects I have done, it migrated from Swift 3
- Expandable label (collapse text). Like when you see a long paragraph in Facebook with 'see more' button. Its concepts it easy but very hard to create expandable label myself. I understood how it works but I decided to user 3rd party to save my time.