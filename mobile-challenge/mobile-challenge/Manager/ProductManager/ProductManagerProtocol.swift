//
//  ProductManagerProtocol.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 17/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

protocol ProductManagerProtocol: class {
    
    func getFavoriteProductIDs() -> [String]
    func addFavoriteProductID(ID: String)
    func removeFavoriteProductID(ID: String)
}
