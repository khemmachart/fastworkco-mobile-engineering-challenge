//
//  FavoriteProductIDManager.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 15/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

class ProductManager: ProductManagerProtocol {
    
    private let userDefaults: UserDefaults
    private let favoriteProductIDsKey = "favorite_product_id"
    
    init(userDefaults: UserDefaults = UserDefaults.standard) {
        self.userDefaults = userDefaults
    }
    
    func getFavoriteProductIDs() -> [String] {
        return userDefaults.stringArray(forKey: favoriteProductIDsKey) ?? []
    }
    
    func addFavoriteProductID(ID: String) {
        var favoriteProductIDs = getFavoriteProductIDs()
        favoriteProductIDs.append(ID)
        userDefaults.set(favoriteProductIDs, forKey: favoriteProductIDsKey)
        userDefaults.synchronize()
    }
    
    func removeFavoriteProductID(ID: String) {
        userDefaults.synchronize()
        userDefaults.set(getFavoriteProductIDs().filter({ $0 != ID }), forKey: favoriteProductIDsKey)
        userDefaults.synchronize()
    }
}
