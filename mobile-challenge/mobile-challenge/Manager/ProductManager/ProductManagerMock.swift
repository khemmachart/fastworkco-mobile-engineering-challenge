//
//  ProductManagerMock.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 16/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

class ProductManagerMock: ProductManagerProtocol {
    
    func getFavoriteProductIDs() -> [String] {
        return [
            "dce7dc92-7bc4-4eba-ad9a-5d7f848a4605",
            "ebfeec86-e3a3-4e24-bf5f-6efba4beee51",
            "21993fad-7a34-4e2f-aa98-e7df516b8585",
            "0d58d781-43fb-46ac-8825-cc6e4fcf1c48"
        ]
    }
    
    func addFavoriteProductID(ID: String) {
        
    }
    
    func removeFavoriteProductID(ID: String) {
        
    }
}
