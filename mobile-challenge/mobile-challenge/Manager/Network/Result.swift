//
//  Result.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/12/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

public enum Result<Value> {
    
    case success(Value)
    case failure(Error)
    
    public init(_ error: Error) {
        self = .failure(error)
    }
    
    public init(_ value: Value) {
        self = .success(value)
    }
    
    public var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .failure:
            return false
        }
    }
    
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
}
