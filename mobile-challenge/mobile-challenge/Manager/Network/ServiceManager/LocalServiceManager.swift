//
//  LocalServiceManager.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 15/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

final class LocalServiceManager: ServiceManagerProtocol {
    
    func request<T: Codable>(
        dataRequest: ServiceDataRequestProtocol,
        completion: @escaping CompletionHandler<T>) -> URLSessionDataTask? {
        
        // Return an error if the url request is incompelted
        guard let localFileUrl = dataRequest.localFileUrl else {
            completion(.failure(UncompletedURLRequestError()))
            return nil
        }
        
        // Requesting service and parsing data
        return URLSession.shared.dataTask(
            with: localFileUrl,
            completionHandler: getRequestCompletionHandler(dataRequest: dataRequest, completion: completion))
    }
}
