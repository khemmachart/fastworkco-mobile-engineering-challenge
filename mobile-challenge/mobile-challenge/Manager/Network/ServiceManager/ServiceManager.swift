//
//  ServiceRequestManager.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/12/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

final class ServiceManager: ServiceManagerProtocol {
    
    func request<T: Codable>(
        dataRequest: ServiceDataRequestProtocol,
        completion: @escaping CompletionHandler<T>) -> URLSessionDataTask? {
        
        // Return an error if the url request is incompelted
        guard let urlRequest = dataRequest.urlRequest else {
            completion(.failure(UncompletedURLRequestError()))
            return nil
        }
        
        // Requesting service and parsing data
        return URLSession.shared.dataTask(
            with: urlRequest,
            completionHandler: getRequestCompletionHandler(dataRequest: dataRequest, completion: completion))
    }
}
