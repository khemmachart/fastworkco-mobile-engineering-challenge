//
//  ServiceManagerProtocol.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 15/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

protocol ServiceManagerProtocol {
    
    // Service completion handler type. Pass the response to result type
    typealias CompletionHandler<T: Codable> = ((Result<T>) -> Void)
    
    // The response object type is depend on the generictype assign by interacter
    func request<T: Codable>(
        dataRequest: ServiceDataRequestProtocol,
        completion: @escaping CompletionHandler<T>) -> URLSessionDataTask?
}

extension ServiceManagerProtocol {
    
    func getRequestCompletionHandler<T: Codable>(
        dataRequest: ServiceDataRequestProtocol,
        completion: @escaping CompletionHandler<T>) -> ((Data?, URLResponse?, Error?) -> Void) {
        
        return { (data, urlResponse, error) in
            self.logResponse(data, dataRequest, urlResponse, error)
            self.performResponse(completion: completion, data, urlResponse, error)
        }
    }
    
    private func performResponse<T: Codable>(
        completion: @escaping CompletionHandler<T>,
        _ data: Data?,
        _ urlResponse: URLResponse?,
        _ error: Error?) {
        
        DispatchQueue.main.async {
            if let unwrappedData = data {
                do {
                    let parsedObject = try JSONDecoder().decode(T.self, from: unwrappedData)
                    completion(.success(parsedObject))
                } catch {
                    completion(.failure(error))
                }
            } else if let unwrappedError = error {
                completion(.failure(unwrappedError))
            }
        }
    }
    
    private func logResponse(_ data: Data?, _ dataRequest: ServiceDataRequestProtocol, _ urlResponse: URLResponse?, _ error: Error?) {
        
        #if DEBUG
        guard let unwrappedData = data else { return }
        guard let httpResponse = urlResponse as? HTTPURLResponse else { return }
        
        do {
            let data = try JSONSerialization.jsonObject(with: unwrappedData, options: [])
            let pretty = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            
            print("-------------------------- ServiceManager --------------------------")
            
            // Method and endpoint
            if let urlString = httpResponse.url?.absoluteString {
                print(dataRequest.httpMethod.rawValue + ": " + urlString)
            }
            
            // Status
            let statusCode = httpResponse.statusCode
            let status = HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
            print("Status: \(statusCode) -  \(status)")
            
            // HttpHeaders
            if let headers = httpResponse.allHeaderFields as? [String: AnyObject] {
                print("Headers: [")
                for (key, value) in headers {
                    print("  \(key): \(value)")
                }
                print("]")
            }
            
            // HttpBody
            print("Body: [")
            if let httpBody = dataRequest.parameters {
                for (key, value) in httpBody {
                    print("  \(key): \(value)")
                }
            }
            print("]")
            
            // Response
            if let string = NSString(data: pretty, encoding: String.Encoding.utf8.rawValue) {
                print("Response: \(string)")
            }
            
            print("--------------------------------------------------------------------")
            
        } catch {
            
        }
        #endif
    }
}
