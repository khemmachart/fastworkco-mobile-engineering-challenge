//
//  UncompletedURLRequestError.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/12/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

struct UncompletedURLRequestError: LocalizedError {
    
    var errorDescription: String? {
        return "The url request is uncompleted with its properties"
    }
}
