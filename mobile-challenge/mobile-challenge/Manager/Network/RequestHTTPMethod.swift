//
//  RequestMethod.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/12/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

enum RequestHTTPMethod: String {
    
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
