//
//  ServiceResponse.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/12/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

struct ServiceResponse<T: Codable>: Codable {
    
    let code: Int?
    let message: String?
    let data: T?
    
    enum CodingKeys: String, CodingKey {
        case code
        case message
        case data
    }
}
