//
//  ServiceDataRequestProtocol.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/12/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

protocol ServiceDataRequestProtocol {
    
    // Required
    var urlString: String { get }
    var httpMethod: RequestHTTPMethod { get }
    
    // Optional (will be replaced with extension protocol)
    var serviceHeaders: [String: String] { get }
    var parameters: [String: Any]? { get }
    var localResponseFilename: String? { get }
}

extension ServiceDataRequestProtocol {
    
    var defaultHeaders: [String: String] {
        return ["Content-Type": "Application/json"]
    }
    
    var localResponseFilename: String? {
        return nil
    }
    
    var parameters: [String: Any]? {
        return nil
    }
    
    var serviceHeaders: [String: String] {
        return [:]
    }
    
    var baseUrlString: String? {
        return "https://www.mocky.io"
    }
    
    var urlRequest: URLRequest? {
        
        // Unwrapping optional
        guard let baseUrlString = baseUrlString else { return nil }
        guard let serviceUrl = URL(string: baseUrlString + urlString) else { return nil }
        
        // Building URLRequest with its properties
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = httpMethod.rawValue
        
        // Http body
        if let parameters = parameters {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        }
        
        // Assign request headers
        for header in defaultHeaders {
            request.setValue(header.value, forHTTPHeaderField: header.key)
        }
        for header in serviceHeaders {
            request.setValue(header.value, forHTTPHeaderField: header.key)
        }
        
        return request
    }
    
    var localFileUrl: URL? {
        if let path = Bundle.main.path(forResource: localResponseFilename, ofType: "json") {
            return URL(fileURLWithPath: path)
        }
        return nil
    }
}

