//
//  ProductListServiceDataRequest.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/12/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

class ProductListServiceDataRequest: ServiceDataRequestProtocol {
    
    var urlString: String = "/v2/5b0275b83000007500cee151"
    var httpMethod: RequestHTTPMethod = .get
    var localResponseFilename = "ProductList"
}
