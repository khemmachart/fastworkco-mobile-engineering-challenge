//
//  ProductDetailSendMessageButtonProtocol.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/23/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

protocol ProductDetailSendMessageButtonProtocol: class {
    
    func integrate(to view: ProductDetailSendMessageButtonDelegate?)
}

protocol ProductDetailSendMessageButtonDelegate: class {
    
    var productDetailSendMessageButtonContainerView: UIView? { set get }
    var productDetailSendMessageButton: ProductDetailSendMessageButtonProtocol? { set get }
    
    func productDetailSendMessageButtonDidPress()
}
