
//
//  ProductDetailSendMessageButton.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/23/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailSendMessageButton: UIButton {
    
    @IBOutlet private weak var buttonContainerView: UIView?
    
    weak var delegate: ProductDetailSendMessageButtonDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
        setButtonTarget()
    }
    
    private func setInterface() {
        buttonContainerView?.layer.borderWidth = 2
        buttonContainerView?.layer.borderColor = UIColor.red.cgColor
    }
    
    private func setButtonTarget() {
        addTarget(self, action: #selector(sendMessageButtonDidPress(_:)), for: .touchUpInside)
    }
    
    @objc private func sendMessageButtonDidPress(_ sender: UIButton) {
        delegate?.productDetailSendMessageButtonDidPress()
    }
}

extension ProductDetailSendMessageButton: ProductDetailSendMessageButtonProtocol {
    
    func integrate(to view: ProductDetailSendMessageButtonDelegate?) {
        view?.productDetailSendMessageButton = self
        view?.productDetailSendMessageButtonContainerView?.addSubview(self)
        view?.productDetailSendMessageButtonContainerView?.constrainViewEqual(to: self)
        delegate = view
    }
}
