//
//  ProductDetailViewModel.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 18/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

class ProductDetailViewModel {
    
    private var product: Product
    private var collapsed: Bool
    
    init(product: Product) {
        self.product = product
        self.collapsed = true
    }
    
    // MARK: - Accessors
    
    func setCollapseStatus(_ status: Bool) {
        collapsed = status
    }
    
    func getNumberOfSections() -> Int {
        return ProductDetailView.Sections.totalSectionNumber
    }
    
    func getNumberOfRowsInSection(section: Int) -> Int? {
        return ProductDetailView.Sections(rawValue: section)?.numberOfRows
    }
    
    func getProductDetailTableViewCellViewModel(at indexPath: IndexPath) -> ProductDetailTableViewCellViewModel {
        return ProductDetailTableViewCellViewModel(product: product, indexPath: indexPath, collapsed: collapsed)
    }
}

