//
//  ProductDetailPhotoTableViewCell.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 18/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailPhotoTableViewCell: ProductDetailTableViewCell {
    
    @IBOutlet private weak var imagesScrollView: UIScrollView?
    @IBOutlet private weak var imagesContentView: UIView?
    @IBOutlet private weak var imagesContentViewWidthConstraint: NSLayoutConstraint?
    @IBOutlet private weak var imagePageControl: UIPageControl?
    
    override func setContent() {
        layoutIfNeeded()
        setImageScrollView()
        setImagePageControl()
    }
    
    private func setImagePageControl() {
        if let photos = viewModel?.getProduct().photos {
            imagePageControl?.numberOfPages = photos.count
            imagePageControl?.currentPage = 0
            imagePageControl?.tintColor = UIColor.red
            imagePageControl?.pageIndicatorTintColor = UIColor.black
            imagePageControl?.currentPageIndicatorTintColor = UIColor.green
        }
    }
    
    private func setImageScrollView() {
        guard let photos = viewModel?.getProduct().photos else { return }
        
        let urlStrings = getThumbnailImageUrlsString(from: photos)
        let thumbnailUrls = getThumnailImageUrls(from: urlStrings)
        let imageViews = getImageViewLists(from: thumbnailUrls)
        
        for imageView in imageViews {
            imagesContentView?.addSubview(imageView)
        }
        
        imagesScrollView?.contentSize.width = UIScreen.main.bounds.width * CGFloat(photos.count)
        imagesScrollView?.isPagingEnabled = true
        imagesScrollView?.delegate = self
        imagesScrollView?.showsHorizontalScrollIndicator = false
        imagesContentViewWidthConstraint?.constant = UIScreen.main.bounds.width * CGFloat(photos.count)
    }
    
    private func getThumbnailImageUrlsString(from photos: [ProductPhoto]) -> [String] {
        return photos.compactMap({ photo in
            return photo.imageThumbnailUrlString
        })
    }
    
    private func getThumnailImageUrls(from urlStrings: [String]) -> [URL] {
        return urlStrings.compactMap({ urlString in
            return URL(string: urlString)
        })
    }
    
    private func getImageViewLists(from urls: [URL]) -> [UIImageView] {
        return urls.enumerated().compactMap({ (index, url) in
            let statingPosition = UIScreen.main.bounds.width * CGFloat(index)
            let origin = CGPoint(x: statingPosition, y: 0)
            let size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * 2 / 5)
            let frame = CGRect(origin: origin, size: size)
            let imageView = CachableImageView(frame: frame)
            imageView.contentMode = .scaleAspectFill
            imageView.setImage(for: url)
            return imageView
        })
    }
}

extension ProductDetailPhotoTableViewCell: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x / bounds.width)
        imagePageControl?.currentPage = Int(pageIndex)
    }
}
