//
//  ProductDetailDescriptionTableViewCell.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 18/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailInformationTableViewCell: ProductDetailTableViewCell {
    
    @IBOutlet private weak var infoImageView: UIImageView?
    @IBOutlet private weak var titleLabel: UILabel?
    @IBOutlet private weak var detailLabel: UILabel?
    
    override func setContent() {
        guard let viewModel = viewModel else { return }
        infoImageView?.image = viewModel.infoImage
        titleLabel?.text = viewModel.productDetailDescriptionDict[viewModel.getIndexPath().row].title
        detailLabel?.text = viewModel.productDetailDescriptionDict[viewModel.getIndexPath().row].detail
    }
}
