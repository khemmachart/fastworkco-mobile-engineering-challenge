//
//  ProductDetailItem.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 18/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailTableViewCell: UITableViewCell {
    
    weak var delegate: ProductDetailTableViewCellDelegate?
    weak var expandableLabelDelegate: ExpandableLabelDelegate?
    
    var viewModel: ProductDetailTableViewCellViewModel? {
        didSet {
            setContent()
            setInterface()
        }
    }
    
    func setInterface() {
        
    }
    
    func setContent() {
        
    }
}
