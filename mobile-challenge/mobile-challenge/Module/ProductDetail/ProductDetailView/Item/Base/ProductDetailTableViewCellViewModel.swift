//
//  ProductDetailTableViewCellViewModel.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 20/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailTableViewCellViewModel {
    
    private var product: Product
    private var indexPath: IndexPath
    private var collapsed: Bool
    
    // This JSON should be included in the response
    let productDetailDescriptionDict: [(title: String, detail: String)] = [
        ("เวลาที่ใช้ในการทำงาน", "2 - 10 วัน"),
        ("มีคนเข้าชม", "1,000"),
        ("จำนวนขายได้", "7 ครั้ง"),
        ("งานสำเร็จ", "100%"),
        ("ถูกใจ", "1,000 คน")
    ]
    
    init(product: Product, indexPath: IndexPath, collapsed: Bool) {
        self.product = product
        self.indexPath = indexPath
        self.collapsed = collapsed
    }
    
    // MARK: - Getters
    
    func getProduct() -> Product {
        return product
    }
    
    func getIndexPath() -> IndexPath {
        return indexPath
    }
    
    func isCollapsed() -> Bool {
        return collapsed
    }
    
    var ratingImage: UIImage? {
        return UIImage(named: "product_rating_\(product.rating ?? 0)")
    }
    
    var userNameText: String? {
        return product.user?.firstName
    }
    
    var profileImageUrl: URL? {
        if let imageUrlString = product.user?.imageUrlString {
            return URL(string: imageUrlString)
        }
        return nil
    }
    
    var purchaseNumberText: String? {
        if let purchase = product.purchase {
            return "\(purchase)"
        }
        return nil
    }
    
    var produtTitleText: String? {
        return product.title
    }
    
    var infoImage: UIImage? {
        return UIImage(named: "ProductInfoImage_\(indexPath.row)")
    }
}
