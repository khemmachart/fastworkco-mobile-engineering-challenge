//
//  ProductDetailDescriptionTableViewCell.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 18/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailDescriptionTableViewCell: ProductDetailTableViewCell {
    
    @IBOutlet private weak var productDescriptionLabel: ExpandableLabel?
    
    override func setInterface() {
        productDescriptionLabel?.delegate = expandableLabelDelegate
        productDescriptionLabel?.setLessLinkWith(lessLink: "ย่อ", attributes: [.foregroundColor:UIColor.gray], position: .left)
        
        layoutIfNeeded()
        
        productDescriptionLabel?.shouldCollapse = true
        productDescriptionLabel?.textReplacementType = .character
        productDescriptionLabel?.numberOfLines = 3
        productDescriptionLabel?.collapsed = viewModel?.isCollapsed() ?? false
        productDescriptionLabel?.text = viewModel?.getProduct().description
    }
    
    override func setContent() {
        productDescriptionLabel?.text = viewModel?.getProduct().description
    }
}
