//
//  ProductDetailUserInformationTableViewCell.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 18/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailUserInformationTableViewCell: ProductDetailTableViewCell {
    
    @IBOutlet private weak var profileImageView: CachableImageView?
    @IBOutlet private weak var nameLabel: UILabel?
    @IBOutlet private weak var onlineStatusLabel: UILabel?

    @IBOutlet private weak var ratingImageView: UIImageView?
    @IBOutlet private weak var purchaseNumberLabel: UILabel?
    
    override func setContent() {
        profileImageView?.setImage(for: viewModel?.profileImageUrl)
        nameLabel?.text = viewModel?.userNameText
        ratingImageView?.image = viewModel?.ratingImage
        purchaseNumberLabel?.text = viewModel?.purchaseNumberText
    }
}
