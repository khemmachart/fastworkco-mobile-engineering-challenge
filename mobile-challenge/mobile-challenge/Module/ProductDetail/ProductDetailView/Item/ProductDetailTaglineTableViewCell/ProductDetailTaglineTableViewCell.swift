//
//  ProductDetailTaglineTableViewCell.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 18/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailTaglineTableViewCell: ProductDetailTableViewCell {
    
    @IBOutlet private weak var taglineLabel: UILabel?
    
    override func setContent() {
        let attributedText = NSMutableAttributedString()
        attributedText.append(firstTaglineAttributedString)
        attributedText.append(secondTaglineAttributedString)
        taglineLabel?.attributedText = attributedText
    }
    
    // MARK: - Interface
    
    private let firstTaglineText: String = "มั่นใจทุกการจ้างงาน"
    private let secondTaglineText: String = ", Fastwork จะเป็นตัวกลางถือเงินจนกว่าคุณจะได้รับงานจากฟรีแลนซ์"
    
    private var firstTaglineAttributedString: NSAttributedString {
        return NSAttributedString(string: firstTaglineText, attributes: highlightTextAttribute)
    }
    
    private var secondTaglineAttributedString: NSAttributedString {
        return NSAttributedString(string: secondTaglineText, attributes: normalTextAttribute)
    }
    
    private var normalTextAttribute: [NSAttributedString.Key: Any] {
        return [
            NSAttributedString.Key.foregroundColor: UIColor.lightGray,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)
        ]
    }
    
    private var highlightTextAttribute: [NSAttributedString.Key: Any] {
        return [
            NSAttributedString.Key.foregroundColor: UIColor.blue,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)
        ]
    }
}
