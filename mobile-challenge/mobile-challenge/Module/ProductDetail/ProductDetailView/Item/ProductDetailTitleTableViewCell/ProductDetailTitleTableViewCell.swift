
//
//  ProductDetailTitleTableVIewCell.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 22/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailTitleTableViewCell: ProductDetailTableViewCell {
    
    @IBOutlet private weak var productTitleLabel: UILabel?
    
    override func setContent() {
        productTitleLabel?.text = viewModel?.produtTitleText
    }
}
