//
//  ProductDetailView+TableView.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 18/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

extension ProductDetailView {
    
    enum Sections: Int, CaseIterable {
        case productPhoto
        case userInformation
        case productTitle
        case productDescription
        case productInformation
        case tagline
        
        static var totalSectionNumber: Int {
            return Sections.tagline.rawValue + 1
        }
        
        var cellIdentifier: String {
            switch self {
            case .productPhoto:
                return "ProductDetailPhotoTableViewCell"
            case .userInformation:
                return "ProductDetailUserInformationTableViewCell"
            case .productTitle:
                return "ProductDetailTitleTableViewCell"
            case .productDescription:
                return "ProductDetailDescriptionTableViewCell"
            case .productInformation:
                return "ProductDetailInformationTableViewCell"
            case .tagline:
                return "ProductDetailTaglineTableViewCell"
            }
        }
        
        var cellNib: UINib {
            return UINib(nibName: cellIdentifier, bundle: nil)
        }
        
        var numberOfRows: Int {
            switch self {
            case .productPhoto:
                return 1
            case .userInformation:
                return 1
            case .productTitle:
                return 1
            case .productDescription:
                return 1
            case .productInformation:
                return 5
            case .tagline:
                return 1
            }
        }
        
        func getCell(tableView: UITableView, indexPath: IndexPath) -> ProductDetailTableViewCell? {
            if let productDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ProductDetailTableViewCell {
                return productDetailTableViewCell
            }
            return nil
        }
    }
}
