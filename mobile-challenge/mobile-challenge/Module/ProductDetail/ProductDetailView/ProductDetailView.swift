//
//  ProductDetailView.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 18/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailView: UIView {
    
    private var productDetailTableView: UITableView?
    private var footerSection: UIView?
    
    var viewModel: ProductDetailViewModel? {
        didSet {
            productDetailTableView?.reloadData()
        }
    }
    
    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setTableViewProperties()
    }
    
    // MARK: - Interface
    
    func setInterface() {
        
    }
    
    func setTableViewProperties() {
        guard productDetailTableView == nil else { return }
        
        productDetailTableView = UITableView(frame: bounds)
        productDetailTableView?.dataSource = self
        productDetailTableView?.estimatedRowHeight = 156
        productDetailTableView?.rowHeight = UITableView.automaticDimension
        productDetailTableView?.separatorStyle = .none
        productDetailTableView?.allowsSelection = false
        
        for section in Sections.allCases {
            productDetailTableView?.register(section.cellNib, forCellReuseIdentifier: section.cellIdentifier)
        }
        
        if let tableView = productDetailTableView {
            addSubview(tableView)
        }
    }
}

extension ProductDetailView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.getNumberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getNumberOfRowsInSection(section: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Sections(rawValue: indexPath.section)?.getCell(tableView: tableView, indexPath: indexPath)
        cell?.delegate = self
        cell?.expandableLabelDelegate = self
        cell?.viewModel = viewModel?.getProductDetailTableViewCellViewModel(at: indexPath)
        return cell ?? UITableViewCell()
    }
}

extension ProductDetailView: ProductDetailTableViewCellDelegate {
    
}

extension ProductDetailView: ExpandableLabelDelegate {
    
    func willExpandLabel(_ label: ExpandableLabel) {
        productDetailTableView?.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        viewModel?.setCollapseStatus(false)
        productDetailTableView?.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        productDetailTableView?.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        viewModel?.setCollapseStatus(true)
        productDetailTableView?.endUpdates()
    }
}
