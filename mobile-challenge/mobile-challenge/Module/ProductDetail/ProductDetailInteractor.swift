//
//  ProductDetailInteractor.swift
//  mobile-challenge
//
//  Created Khemmachart Chutapetch on 11/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class ProductDetailInteractor {
    
    let productManager: ProductManagerProtocol?

    weak var presenter: ProductDetailInteractorOutputProtocol?
    
    init(productManager: ProductManagerProtocol?) {
        self.productManager = productManager
    }
}

extension ProductDetailInteractor: ProductDetailInteractorInputProtocol {
    
    func addFavoriteProductID(ID: String) {
        productManager?.addFavoriteProductID(ID: ID)
        presenter?.addFavoriteProductDidSuccess()
    }
    
    func removeFavoriteProductID(ID: String) {
        productManager?.removeFavoriteProductID(ID: ID)
        presenter?.removeFavoriteProductDidSuccess()
    }
}
