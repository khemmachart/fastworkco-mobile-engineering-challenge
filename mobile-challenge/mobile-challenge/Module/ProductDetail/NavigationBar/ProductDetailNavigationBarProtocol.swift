//
//  ProductDetailNavigationBarProtocol.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/22/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

protocol ProductDetailNavigationBarProtocol: class {
    
    var delegate: ProductDetailNavigationBarDelegate? { set get }

    func setTitle(title: String?)
    func integrate(to view: ProductDetailNavigationBarDelegate?)
}

protocol ProductDetailNavigationBarDelegate: class {
    
    var navigationBarContainerView: UIView? { set get }
    var navigationBar: ProductDetailNavigationBarProtocol? { set get }
    
    func backButtonDidPress()
    func moreButtonDidPress()
}
