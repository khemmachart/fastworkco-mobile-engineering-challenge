//
//  ProductDetailNavigationBar.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/22/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailNavigationBar: UIView {
    
    @IBOutlet private weak var titleLabel: UILabel?
    
    weak var delegate: ProductDetailNavigationBarDelegate?
    
    @IBAction private func backButtonDidPress(_ sender: UIButton) {
        delegate?.backButtonDidPress()
    }
    
    @IBAction private func moreButtonDidPress(_ sender: UIButton) {
        delegate?.moreButtonDidPress()
    }
}

extension ProductDetailNavigationBar: ProductDetailNavigationBarProtocol {
    
    func setTitle(title: String?) {
        titleLabel?.text = title
    }

    func integrate(to view: ProductDetailNavigationBarDelegate?) {
        view?.navigationBar = self
        view?.navigationBarContainerView?.addSubview(self)
        view?.navigationBarContainerView?.constrainViewEqual(to: self)
        delegate = view
    }
}
