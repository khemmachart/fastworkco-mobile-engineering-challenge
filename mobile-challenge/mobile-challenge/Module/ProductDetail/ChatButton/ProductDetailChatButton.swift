//
//  ProductDetailChatButton.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/23/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailChatButton: UIButton {
    
    @IBOutlet private weak var buttonContainerView: UIView?
    
    weak var delegate: ProductDetailChatButtonDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
        setButtonTarget()
    }
    
    private func setInterface() {
        buttonContainerView?.layer.borderWidth = 2
        buttonContainerView?.layer.borderColor = UIColor(hexString: "E7E7E7").cgColor
    }
    
    private func setButtonTarget() {
        addTarget(self, action: #selector(chatButtonDidPress(_:)), for: .touchUpInside)
    }
    
    @objc private func chatButtonDidPress(_ sender: UIButton) {
        delegate?.productDetailChatButtonDidPress()
    }
}

extension ProductDetailChatButton: ProductDetailChatButtonProtocol {
    
    func integrate(to view: ProductDetailChatButtonDelegate?) {
        view?.productDetailChatButton = self
        view?.productDetailChatButtonContainerView?.addSubview(self)
        view?.productDetailChatButtonContainerView?.constrainViewEqual(to: self)
        delegate = view
    }
}
