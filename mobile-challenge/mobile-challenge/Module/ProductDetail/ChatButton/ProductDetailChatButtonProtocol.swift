//
//  ProductDetailChatButtonProtocol.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/23/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

protocol ProductDetailChatButtonProtocol: class {
    
    func integrate(to view: ProductDetailChatButtonDelegate?)
}

protocol ProductDetailChatButtonDelegate: class {
    
    var productDetailChatButtonContainerView: UIView? { set get }
    var productDetailChatButton: ProductDetailChatButtonProtocol? { set get }
    
    func productDetailChatButtonDidPress()
}
