//
//  ProductDetailFavoriteButtonProtocol.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/23/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

protocol ProductDetailFavoriteButtonProtocol: class {
    
    func integrate(to view: ProductDetailFavoriteButtonDelegate?)
}

protocol ProductDetailFavoriteButtonDelegate: class {
    
    var productDetailFavoriteButtonContainerView: UIView? { set get }
    var productDetailFavoriteButton: ProductDetailFavoriteButton? { set get }
    
    func productDetailFavoriteButtonDidPress()
}
