//
//  ProductDetailFavoriteButton.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/23/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductDetailFavoriteButton: UIButton {
    
    @IBOutlet private weak var favoriteImageView: UIImageView?
    
    weak var delegate: ProductDetailFavoriteButtonDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addTarget(self, action: #selector(favoriteButtonDidPress(_:)), for: .touchUpInside)
    }
    
    func setIsFavorite(_ isFavorite: Bool) {
        if isFavorite {
            favoriteImageView?.image = UIImage(named: "product_favorite_icon_active")
        } else {
            favoriteImageView?.image = UIImage(named: "product_favorite_icon_inactive")
        }
    }
    
    @objc func favoriteButtonDidPress(_ sender: UIButton) {
        delegate?.productDetailFavoriteButtonDidPress()
    }
}

extension ProductDetailFavoriteButton: ProductDetailFavoriteButtonProtocol {
    
    func integrate(to view: ProductDetailFavoriteButtonDelegate?) {
        view?.productDetailFavoriteButton = self
        view?.productDetailFavoriteButtonContainerView?.addSubview(self)
        view?.productDetailFavoriteButtonContainerView?.constrainViewEqual(to: self)
        delegate = view
    }
}
