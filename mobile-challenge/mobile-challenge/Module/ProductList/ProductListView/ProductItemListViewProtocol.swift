//
//  ProductListViewProtocol.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 16/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

protocol ProductItemListViewProtocol: class {
    
    var delegate: ProductItemListViewDelegate? { set get }
    var viewModel: ProductListViewModel? { set get }
    
    func integrate(to view: ProductItemListViewDelegate?)
}

protocol ProductItemListViewDelegate: class {
 
    var productItemListContainerView: UIView? { set get }
    var productItemListView: ProductItemListViewProtocol? { set get }
    
    func productFavoriteButtonDidPress(product: Product)
    func productSendingMessageButtonDidPress(product: Product)
    func productItemDidSelect(product: Product)
}
