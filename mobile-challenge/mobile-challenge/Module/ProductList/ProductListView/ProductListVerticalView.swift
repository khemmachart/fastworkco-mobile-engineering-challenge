//
//  ProductListView.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 16/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductListVerticalView: UIView {
    
    @IBOutlet private weak var productTableView: UITableView?
    
    var viewModel: ProductListViewModel? {
        didSet {
            productTableView?.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTableViewProperties()
    }
    
    weak var delegate: ProductItemListViewDelegate?
    
    // MARK: - Interface
    
    private func setTableViewProperties() {
        productTableView?.dataSource = self
        productTableView?.estimatedRowHeight = 52
        productTableView?.rowHeight = UITableView.automaticDimension
        
        let productItemCell = Sections.productItems
        productTableView?.register(productItemCell.cellNib, forCellReuseIdentifier: productItemCell.cellIdentifier)
    }
}

extension ProductListVerticalView: ProductItemListViewProtocol {

    func integrate(to view: ProductItemListViewDelegate?) {
        view?.productItemListView = self
        view?.productItemListContainerView?.addSubview(self)
        view?.productItemListContainerView?.constrainViewEqual(to: self)
        delegate = view
    }
}

extension ProductListVerticalView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRowsInSection(section: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Sections(rawValue: indexPath.section)?.getCell(tableView: tableView, indexPath: indexPath)
        cell?.viewModel = viewModel?.getProductTableViewCellViewModel(at: indexPath.row)
        cell?.delegate = self
        return cell ?? UITableViewCell()
    }
}

extension ProductListVerticalView: ProductTableViewCellDelegate {
    
    func productFavoriteButtonDidPress(product: Product) {
        delegate?.productFavoriteButtonDidPress(product: product)
    }
    
    func productSendingMessageButtonDidPress(product: Product) {
        delegate?.productSendingMessageButtonDidPress(product: product)
    }
    
    func productItemDidSelect(product: Product) {
        delegate?.productItemDidSelect(product: product)
    }
}
