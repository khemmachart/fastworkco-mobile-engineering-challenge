//
//  ProductListVerticalView+Sections.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 19/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

extension ProductListVerticalView {
    
    enum Sections: Int {
        
        case productItems
        
        static var totalSection: Int {
            return Sections.productItems.rawValue + 1
        }
        
        var cellIdentifier: String {
            return "ProductTableViewCell"
        }
        
        var cellNib: UINib {
            return UINib(nibName: cellIdentifier, bundle: nil)
        }
        
        func getCell(tableView: UITableView, indexPath: IndexPath) -> ProductTableViewCell? {
            if let productTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ProductTableViewCell {
                return productTableViewCell
            }
            return nil
        }
    }
}
