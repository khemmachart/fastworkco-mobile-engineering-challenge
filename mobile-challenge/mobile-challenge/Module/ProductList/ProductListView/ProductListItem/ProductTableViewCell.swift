//
//  ProductTableViewCell.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit
import SDWebImage

class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var productCoverPhotoImageView: CachableImageView?
    @IBOutlet private weak var productProfilePhotoImageView: CachableImageView?
    
    @IBOutlet private weak var userNameLabel: UILabel?
    @IBOutlet private weak var onlineStatusLabel: UILabel?
    @IBOutlet private weak var productTitle: UILabel?
    @IBOutlet private weak var productPrize: UILabel?
    @IBOutlet private weak var separaterLine: UIView?
    @IBOutlet private weak var purchaseLabel: UILabel?
    
    @IBOutlet private weak var productRaringImageView: UIImageView?
    @IBOutlet private weak var favoriteButton: UIButton?
    @IBOutlet private weak var sendingMessageButton: UIButton?
    
    weak var delegate: ProductTableViewCellDelegate?
    var viewModel: ProductTableViewCellViewModel? {
        didSet {
            setContent()
            setFavoriteButton()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
        setButtonTarget()
        setGestureRecognizer()
    }
    
    // MARK: - Interface
    
    private func setInterface() {
        separaterLine?.backgroundColor = UIColor(hexString: "E7E7E7")
        productTitle?.textColor = UIColor(hexString: "959595")
        userNameLabel?.textColor = UIColor(hexString: "959595")
        onlineStatusLabel?.textColor = UIColor(hexString: "959595")
        productPrize?.textColor = UIColor(hexString: "959595")
        purchaseLabel?.textColor = UIColor(hexString: "959595")
    }
    
    private func setContent() {
        productTitle?.text = viewModel?.productTitleText
        userNameLabel?.text = viewModel?.userFirstNameText
        productRaringImageView?.image = viewModel?.ratingImage
        purchaseLabel?.text = viewModel?.productPruchaseText
        productProfilePhotoImageView?.setImage(for: viewModel?.userImageUrl)
        productCoverPhotoImageView?.setImage(for: viewModel?.productCoverUrl)
    }
    
    private func setButtonTarget() {
        favoriteButton?.addTarget(self, action: #selector(favoriteButtonDidPress(_:)), for: .touchUpInside)
        sendingMessageButton?.addTarget(self, action: #selector(sendingMessageButtonDidPress(_:)), for: .touchUpInside)
    }
    
    private func setGestureRecognizer() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(productItemDidSelect(_:)))
        gesture.numberOfTapsRequired = 1
        addGestureRecognizer(gesture)
        isUserInteractionEnabled = true
    }
    
    private func setFavoriteButton() {
        if let isUserFavoriteProduct = viewModel?.isUserFavoriteProduct, isUserFavoriteProduct {
            favoriteButton?.setImage(UIImage(named: "product_favorite_icon_active"), for: .normal)
        } else {
            favoriteButton?.setImage(UIImage(named: "product_favorite_icon_inactive"), for: .normal)
        }
    }
    
    // MARK: - Action
    
    @IBAction private func favoriteButtonDidPress(_ sender: UIButton) {
        if let product = viewModel?.getProduct() {
            delegate?.productFavoriteButtonDidPress(product: product)
        }
    }
    
    @IBAction private func sendingMessageButtonDidPress(_ sender: UIButton) {
        if let product = viewModel?.getProduct() {
            delegate?.productSendingMessageButtonDidPress(product: product)
        }
    }
    
    @IBAction private func productItemDidSelect(_ gesture: UIGestureRecognizer) {
        if let product = viewModel?.getProduct() {
            delegate?.productItemDidSelect(product: product)
        }
    }
}
