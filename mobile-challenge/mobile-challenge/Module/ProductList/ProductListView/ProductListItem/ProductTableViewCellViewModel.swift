//
//  ProductTableViewCellViewModel.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 19/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductTableViewCellViewModel {
    
    private var product: Product?
    private var coverUrl: URL?
    
    init(product: Product?, coverUrl: URL?) {
        self.product = product
        self.coverUrl = coverUrl
    }
    
    // MARK: - Getters
    
    func getProduct() -> Product? {
        return product
    }
    
    var isUserFavoriteProduct: Bool? {
        return product?.isUserFavorite
    }
    
    var productTitleText: String? {
        return product?.title
    }
    
    var userFirstNameText: String? {
        return product?.user?.firstName
    }
    
    var ratingImage: UIImage? {
        return UIImage(named: "product_rating_\(product?.rating ?? 0)")
    }

    var productPruchaseText: String? {
        return "(\(product?.purchase ?? 0))"
    }
    
    var userImageUrl: URL? {
        if let userImageUrlString = product?.user?.imageUrlString {
            return URL(string: userImageUrlString)
        }
        return nil
    }
    
    var productCoverUrl: URL? {
        return coverUrl
    }
}
