//
//  ProductContentContainerView.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 15/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductContainerView: RoundRectsView {
    
    override func setupInterface() {
        super.setupInterface()
        layer.borderWidth = 2
        layer.borderColor = UIColor(hexString: "E7E7E7").cgColor
        backgroundColor = .white
    }
}
