//
//  ProductTableViewCellProtocol.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/15/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

protocol ProductTableViewCellDelegate: class {
    
    func productFavoriteButtonDidPress(product: Product)
    func productSendingMessageButtonDidPress(product: Product)
    func productItemDidSelect(product: Product)
}
