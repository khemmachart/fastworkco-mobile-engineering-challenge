//
//  ProductListViewModel.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 19/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

class ProductListViewModel {
    
    private let products: [Product]
    private var productCoverImageUrls: [URL] = []
    
    init(products: [Product]) {
        self.products = products
        self.productCoverImageUrls = getProductCoverImageUrls(from: products)
    }
    
    func getProductTableViewCellViewModel(at index: Int) -> ProductTableViewCellViewModel {
        return ProductTableViewCellViewModel(
            product: getProduct(at: index),
            coverUrl: getProductCoverImageUrl(at: index))
    }
    
    func getProducts() -> [Product] {
        return products
    }
    
    func getProduct(at index: Int) -> Product? {
        if index >= 0 && index < products.count {
            return products[index]
        }
        return nil
    }
    
    func getProductCoverImageUrls() -> [URL] {
        return productCoverImageUrls
    }
    
    func getProductCoverImageUrl(at index: Int) -> URL? {
        if index >= 0 && index < productCoverImageUrls.count {
            return productCoverImageUrls[index]
        }
        return nil
    }
    
    func getProductCoverImageUrls(from products: [Product]) -> [URL] {
        return getProductCoverImageUrlStringList(from: products).compactMap({ URL(string: $0)})
    }
    
    func getProductCoverImageUrlString(from product: Product?) -> String? {
        return product?.photos.filter({ photo in
            return photo.isCoverPhoto ?? false
        }).first?.imageThumbnailUrlString
    }
    
    func getProductCoverImageUrlStringList(from products: [Product]) -> [String] {
        return products.compactMap({ product in
            return getProductCoverImageUrlString(from: product)
        })
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return products.count
    }
}

