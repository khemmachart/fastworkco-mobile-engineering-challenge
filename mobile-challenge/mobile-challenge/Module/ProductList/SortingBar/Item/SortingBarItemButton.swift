//
//  SortingBarItemButton.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class SortingBarItemButton: UIButton {
    
    @IBOutlet private weak var sortingNameLabel: UILabel?
    @IBOutlet private weak var verticalSeparaterView: UIView?
    @IBOutlet private weak var horizontalSeparaterView: UIView?
    @IBOutlet private weak var activeIndicatorView: UIView?
    
    private var sortingType: SortingBarItemButton.SortingType? {
        didSet {
            setContent()
        }
    }
    
    private var status: Status = .inactive {
        didSet {
            setStatusInterface()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
        setStatusInterface()
    }
    
    // MARK: - Accessors
    
    var isActive: Bool {
        return status == .active
    }
    
    func setActive() {
        status = .active
    }
    
    func setInactive() {
        status = .inactive
    }
    
    func setType(_ value: SortingType) {
        sortingType = value
    }
    
    func getType() -> SortingType? {
        return sortingType
    }
    
    func setShouldDisplatVerticalSeparate(_ value: Bool) {
        verticalSeparaterView?.isHidden = value
    }
    
    func integrate(to view: UIView?) {
        view?.addSubview(self)
        view?.constrainViewEqual(to: self)
    }
    
    // MARK: - Interface
    
    private func setInterface() {
        verticalSeparaterView?.backgroundColor = UIColor(hexString: "E7E7E7")
        horizontalSeparaterView?.backgroundColor = UIColor(hexString: "E7E7E7")
        sortingNameLabel?.textColor = UIColor(hexString: "959595")
    }
    
    private func setStatusInterface() {
        if status == .active {
            setActiveInterface()
        } else {
            setInactiveInterface()
        }
    }
    
    private func setContent() {
        sortingNameLabel?.text = sortingType?.title
    }
    
    private func setActiveInterface() {
        activeIndicatorView?.isHidden = false
    }
    
    private func setInactiveInterface() {
        activeIndicatorView?.isHidden = true
    }
}

extension SortingBarItemButton {
    
    enum Status {
        case active
        case inactive
    }
}

extension SortingBarItemButton {
    
    enum SortingType: Int {
        
        case recommended
        case rating
        case completionRate
        case purchase
        
        var title: String {
            switch self {
            case .recommended:
                return "แนะนำ"
            case .rating:
                return "เรตติ้ง"
            case .completionRate:
                return "งานสำเร็จ"
            case .purchase:
                return "ราคา"
            }
        }
        
        var sortingKey: String {
            return ""
        }
    }
}
