//
//  SortingBarViewProtocol.swift
//  
//
//  Created by KHUN NINE on 1/13/19.
//

import UIKit

protocol SortingBarViewProtocol: class {
    
    var delegate: SortingBarViewDelegate? { set get }
    
    func integrate(to view: SortingBarViewDelegate?)
}

protocol SortingBarViewDelegate: class {
    
    var sortingBarContainerView: UIView? { set get }
    var sortingBarView: SortingBarViewProtocol? { set get }
    
    func sortingBarItemDidPress(type: SortingBarItemButton.SortingType)
}
