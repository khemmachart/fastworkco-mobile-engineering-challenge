//
//  SortingBarView.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class SortingBarView: UIView {
    
    @IBOutlet private var itemContainerViews: [UIView] = []
    
    weak var delegate: SortingBarViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
        setInitialButtonState()
    }
    
    func getActiveItemIndex() -> Int? {
        return getSortingItems().enumerated().filter({ $0.element.isActive }).first?.offset
    }
    
    func getItem(at index: Int) -> SortingBarItemButton? {
        if index >= 0 && index < getSortingItems().count {
            return  getSortingItems()[index]
        }
        return nil
    }
    
    func getSortingItems() -> [SortingBarItemButton] {
        return itemContainerViews.compactMap({ $0.subviews.first as? SortingBarItemButton })
    }
    
    // MARK: - Action
    
    @objc func sortingItemDidSelect(_ selectedItem: SortingBarItemButton) {
        
        guard !selectedItem.isActive else { return }
        guard let type = selectedItem.getType() else { return }
        
        for item in getSortingItems() {
            if item == selectedItem {
                item.setActive()
            } else {
                item.setInactive()
            }
        }
        
        delegate?.sortingBarItemDidPress(type: type)
    }
    
    // MARK: - Interface
    
    func setInterface() {
        for (index, containerView) in itemContainerViews.enumerated() {
            addItemView(for: index, to: containerView, containerViewCount: itemContainerViews.count)
        }
    }
    
    func addItemView(for index: Int, to containerView: UIView, containerViewCount: Int) {
        if let itemViewType = getItemViewType(index: index) {
            let itemView: SortingBarItemButton? = SortingBarItemButton.instanceFromNib()
            itemView?.setType(itemViewType)
            itemView?.setShouldDisplatVerticalSeparate(index == containerViewCount - 1)
            itemView?.integrate(to: containerView)
            itemView?.addTarget(self, action: #selector(sortingItemDidSelect(_:)), for: .touchUpInside)
        }
    }
    
    func getItemViewType(index: Int) -> SortingBarItemButton.SortingType? {
        if let type = SortingBarItemButton.SortingType(rawValue: index) {
            return type
        }
        return nil
    }
    
    func setInitialButtonState() {
        getItem(at: 0)?.setActive()
    }
}

extension SortingBarView: SortingBarViewProtocol {
    
    func integrate(to view: SortingBarViewDelegate?) {
        view?.sortingBarView = self
        view?.sortingBarContainerView?.addSubview(self)
        view?.sortingBarContainerView?.constrainViewEqual(to: self)
        delegate = view
    }
}
