//
//  SearchBarView.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class SearchBarView: UIView {
    
    @IBOutlet private weak var titleLabel: UILabel?
    @IBOutlet private weak var searchTextFieldContainerView: UIView?
    @IBOutlet private weak var searchTextField: UITextField?
    @IBOutlet private weak var filterButton: ProductFilterButton?
    
    weak var delegate: SearchBarViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
    }
    
    // MARK: - Interface
    
    func setInterface() {
        titleLabel?.textColor = UIColor(hexString: "959595")
        searchTextField?.textColor = UIColor(hexString: "959595")
        searchTextFieldContainerView?.layer.borderColor = UIColor(hexString: "D0D0D0").cgColor
        searchTextFieldContainerView?.layer.borderWidth = 1
        backgroundColor = UIColor(hexString: "E7E7E7")
        filterButton?.addTarget(self, action: #selector(filterButtonDidPress(_:)), for: .touchUpInside)
    }
    
    // MARK: - Action
    
    @IBAction func filterButtonDidPress(_ sender: UIButton) {
        delegate?.filterButtonDidPress()
    }
}

extension SearchBarView: SearchBarViewProtocol {
    
    func integrate(to view: SearchBarViewDelegate?) {
        view?.searchBarView = self
        view?.searchBarContainerView?.addSubview(self)
        view?.searchBarContainerView?.constrainViewEqual(to: self)
        delegate = view
    }
}
