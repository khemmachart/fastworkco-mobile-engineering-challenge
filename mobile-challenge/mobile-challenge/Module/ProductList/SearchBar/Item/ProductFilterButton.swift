//
//  ProductFilterButton.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/15/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class ProductFilterButton: UIButton {
    
    @IBOutlet private weak var filterLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
    }
    
    // MAKR: - Interface
    
    private func setInterface() {
        filterLabel?.textColor = UIColor(hexString: "959595")
    }
}
