//
//  SearchBarViewProtocol.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

protocol SearchBarViewProtocol: class {
    
    var delegate: SearchBarViewDelegate? { set get }
}

protocol SearchBarViewDelegate: class {
    
    var searchBarContainerView: UIView? { set get }
    var searchBarView: SearchBarViewProtocol? { set get }
    
    func filterButtonDidPress()
}
