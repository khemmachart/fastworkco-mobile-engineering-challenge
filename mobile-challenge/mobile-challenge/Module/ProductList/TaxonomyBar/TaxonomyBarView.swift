//
//  SortingCollection.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 14/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class TaxonomyBarView: UIView {
    
    @IBOutlet private weak var collectionView: UICollectionView?
    @IBOutlet private weak var separateView: UIView?
    
    private let sortingCollectionViewCellName = "TaxonomyBarItemView"
    private var sortingCollectionItems: [SortingCollectionItem] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
        sortingCollectionItems = [
            SortingCollectionItem(title: "Sizing"),
            SortingCollectionItem(title: "Taxonomy"),
            SortingCollectionItem(title: "collection items based on"),
            SortingCollectionItem(title: "its"),
            SortingCollectionItem(title: "label size")
        ]
    }
    
    // MARK: - Interface
    
    func setInterface() {
        let cell = getProductTableViewCellNib()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.register(cell.cellNib, forCellWithReuseIdentifier: cell.cellReuseIdentifier)
        collectionView?.collectionViewLayout = collectionViewLayout
        collectionView?.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        collectionView?.showsHorizontalScrollIndicator = false
        separateView?.backgroundColor = UIColor(hexString: "E7E7E7")
    }
    
    func getProductTableViewCellNib() -> (cellNib: UINib, cellReuseIdentifier: String) {
        let cellNib = UINib(nibName: sortingCollectionViewCellName, bundle: nil)
        return (cellNib: cellNib, cellReuseIdentifier: sortingCollectionViewCellName)
    }
    
    var collectionViewLayout: UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        return layout
    }
    
    func getItem(at index: Int) -> SortingCollectionItem? {
        if index >= 0 && index < sortingCollectionItems.count {
            return sortingCollectionItems[index]
        }
        return nil
    }
    
    func getWidthForItem(at index: Int) -> CGFloat {
        let defaultWidth: CGFloat = 104
        let itemMargin: CGFloat = ((16 + 8) * 2)
        
        if let title = getItem(at: index)?.title {
            return title.size(withAttributes: [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)
                ]).width + itemMargin
        }
        
        return defaultWidth
    }
    
    func getSizeForItem(at index: Int) -> CGSize {
        let separateHeight: CGFloat = separateView?.bounds.height ?? 0
        return CGSize(width: getWidthForItem(at: index), height: bounds.height - separateHeight)
    }
}

extension TaxonomyBarView: TaxonomyBarViewProtocol {
    
    func integrate(to view: TaxonomyBarViewDelegate?) {
        view?.sortingCollectionContainerView?.addSubview(self)
        view?.sortingCollectionContainerView?.constrainViewEqual(to: self)
    }
}

extension TaxonomyBarView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sortingCollectionItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: sortingCollectionViewCellName, for: indexPath) as? TaxonomyBarItemView,
            let item = getItem(at: indexPath.row) {
            cell.setContent(item: item)
            return cell
        }
        return UICollectionViewCell()
    }
}

extension TaxonomyBarView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return getSizeForItem(at: indexPath.row)
    }
}
