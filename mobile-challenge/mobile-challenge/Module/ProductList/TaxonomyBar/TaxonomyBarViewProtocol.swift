//
//  TaxonomyBarViewProtocol.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 14/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

protocol TaxonomyBarViewProtocol: class {
    
    func integrate(to view: TaxonomyBarViewDelegate?)
}

protocol TaxonomyBarViewDelegate: class {
    
    var sortingCollectionContainerView: UIView? { set get }
    var sortingCollectionView: TaxonomyBarViewProtocol? { set get }
}
