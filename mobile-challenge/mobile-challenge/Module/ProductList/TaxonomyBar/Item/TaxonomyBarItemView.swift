//
//  TaxonomyBarItemView.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 14/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class TaxonomyBarItemView: UICollectionViewCell {
    
    @IBOutlet private weak var nameContainerView: CircularView?
    @IBOutlet private weak var nameTitleLabel: FastworkLabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInterface()
    }
    
    // MARK: - Interface
    
    private func setInterface() {
        backgroundColor = .white
        nameContainerView?.backgroundColor = UIColor(hexString: "E7E7E7")
        nameTitleLabel?.textColor = UIColor(hexString: "959595")
    }
    
    func setContent(item: SortingCollectionItem) {
        nameTitleLabel?.text = item.title
    }
}
