//
//  BaseViewController.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 24/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, ObjectMemoryObservableProtocol {
    
    deinit {
        logDeinitObject()
        removeNotificationObserver()
    }
}
