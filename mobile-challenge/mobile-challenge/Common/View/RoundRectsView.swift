//
//  RoundedView.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class RoundRectsView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInterface()
    }
    
    func setupInterface() {
        layer.cornerRadius = 10
        clipsToBounds = true
    }
}
