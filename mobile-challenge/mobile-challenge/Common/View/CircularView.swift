//
//  CircularView.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

class CircularView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInterface()
    }
    
    private func setupInterface() {
        layer.cornerRadius = min(bounds.width, bounds.height) / 2
        clipsToBounds = true
    }
}
