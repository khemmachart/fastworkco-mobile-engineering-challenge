//
//  CachableImageView.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 15/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit
import SDWebImage

class CachableImageView: UIImageView {
    
    static var defaultDisplayImage = UIImage(named: "ic_profile_empty")
    
    func setImage(
        for url: URL?,
        forceLoad: Bool = false,
        placeholderImage: UIImage? = defaultDisplayImage) {
        
        if forceLoad {
            loadImage(from: url, placeholderImage: placeholderImage)
        } else {
            let handler = handleSetImage(for: url, placeholderImage: placeholderImage)
            SDWebImageManager.shared().cachedImageExists(for: url, completion: handler)
        }
    }
    
    private func handleSetImage(for url: URL?, placeholderImage: UIImage?) -> SDWebImageCheckCacheCompletionBlock {
        return { [weak self] cached in
            if cached {
                self?.loadCachedImage(for: url)
            } else {
                self?.loadImage(from: url, placeholderImage: placeholderImage)
            }
        }
    }
    
    private func loadCachedImage(for url: URL?) {
        image = SDImageCache.shared().imageFromCache(forKey: url?.absoluteString)
    }
    
    private func loadImage(from url: URL?, placeholderImage: UIImage?) {
        sd_setImage(with: url, placeholderImage: placeholderImage)
    }
}
