//
//  UIView+Constraint.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

extension UIView {
    
    func constrainViewEqual(to view: UIView) {
        self.addConstraints(getConstraintsToFitSubview(view: view))
    }
    
    func getConstraintsToFitSubview(view: UIView) -> [NSLayoutConstraint] {
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let leading  = getConstraint(view: view, relatedView: view.superview!, attribute: .leading)
        let trailing = getConstraint(view: view, relatedView: view.superview!, attribute: .trailing)
        let top      = getConstraint(view: view, relatedView: view.superview!, attribute: .top)
        let bottom   = getConstraint(view: view, relatedView: view.superview!, attribute: .bottom)
        
        return [leading, trailing, top, bottom]
    }
    
    func getConstraint(view: UIView, relatedView: UIView, attribute: NSLayoutConstraint.Attribute) -> NSLayoutConstraint {
        return NSLayoutConstraint(
            item: view,
            attribute: attribute,
            relatedBy: NSLayoutConstraint.Relation.equal,
            toItem: relatedView,
            attribute: attribute,
            multiplier: 1,
            constant: 0)
    }
}
