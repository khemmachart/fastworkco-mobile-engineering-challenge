//
//  UIViewController+Notification.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 16/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

extension UIViewController {
    
    public func addNotificationObserver(notificationName: Notification.Name, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: notificationName, object: nil)
    }
    
    public func removeNotificationObserver(notificationName: Notification.Name) {
        NotificationCenter.default.removeObserver(self, name: notificationName, object: nil)
    }
    
    public func removeNotificationObserver() {
        NotificationCenter.default.removeObserver(self)
    }
}
