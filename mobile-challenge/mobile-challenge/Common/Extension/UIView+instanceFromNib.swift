//
//  UIView+instanceFromNib.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

extension UIView {
    
    class func instanceFromNib<T: UIView>(nibName: String = String(describing: T.self)) -> T? {
        return getNib(nibName: nibName)?.instantiate(withOwner: nil, options: nil).first as? T
    }
}
