//
//  UIView+GetNib.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

extension UIView {
    
    class func getNib(nibName: String? = nil) -> UINib? {
        return UINib(nibName: nibName ?? className, bundle: nil)
    }
}
