//
//  UIViewController+KeyboardHandler.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 16/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.

import UIKit

extension UIViewController {
    
    // MARK: - Keyboard notification
    
    public func addKeyboardWillShowNotification() {
        let name = UIWindow.keyboardWillShowNotification
        let selector = #selector(UIViewController.keyboardWillShowNotification(_:))
        addNotificationObserver(notificationName: name, selector: selector)
    }
    
    public func addKeyboardDidShowNotification() {
        let name = UIWindow.keyboardDidShowNotification
        let selector = #selector(UIViewController.keyboardDidShowNotification(_:))
        addNotificationObserver(notificationName: name, selector: selector)
    }
    
    public func addKeyboardWillHideNotification() {
        let name = UIWindow.keyboardWillHideNotification
        let selector = #selector(UIViewController.keyboardWillHideNotification(_:))
        addNotificationObserver(notificationName: name, selector: selector)
    }
    
    public func addKeyboardDidHideNotification() {
        let name = UIWindow.keyboardDidHideNotification
        let selector = #selector(UIViewController.keyboardDidHideNotification(_:))
        addNotificationObserver(notificationName: name, selector: selector)
    }
    
    public func removeKeyboardWillShowNotification() {
        let name = UIWindow.keyboardWillShowNotification
        removeNotificationObserver(notificationName: name)
    }
    
    public func removeKeyboardDidShowNotification() {
        let name = UIWindow.keyboardDidShowNotification
        removeNotificationObserver(notificationName: name)
    }
    
    public func removeKeyboardWillHideNotification() {
        let name = UIWindow.keyboardWillHideNotification
        removeNotificationObserver(notificationName: name)
    }
    
    public func removeKeyboardDidHideNotification() {
        let name = UIWindow.keyboardDidHideNotification
        removeNotificationObserver(notificationName: name)
    }
    
    @objc public func keyboardDidShowNotification(_ notification: NSNotification) {
        if let nInfo = notification.userInfo, let value = nInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let frame = value.cgRectValue
            keyboardDidShow(withFrame: frame)
        }
    }
    
    @objc public func keyboardWillShowNotification(_ notification: NSNotification) {
        if let nInfo = notification.userInfo, let value = nInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let frame = value.cgRectValue
            keyboardWillShow(withFrame: frame)
        }
    }
    
    @objc public func keyboardWillHideNotification(_ notification: NSNotification) {
        if let nInfo = notification.userInfo, let value = nInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let frame = value.cgRectValue
            keyboardWillHide(withFrame: frame)
        }
    }
    
    @objc public func keyboardDidHideNotification(_ notification: NSNotification) {
        if let nInfo = notification.userInfo, let value = nInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let frame = value.cgRectValue
            keyboardDidHide(withFrame: frame)
        }
    }
    
    // MARK: - Keyboard notification handlers
    
    @objc open func keyboardWillShow(withFrame frame: CGRect) {
        // To be overrided
    }
    
    @objc open func keyboardDidShow(withFrame frame: CGRect) {
        // To be overrided
    }
    
    @objc open func keyboardWillHide(withFrame frame: CGRect) {
        // To be overrided
    }
    
    @objc open func keyboardDidHide(withFrame frame: CGRect) {
        // To be overrided
    }
    
    // MARK: - Keyboard
    
    public func hideKeyboardWhenTappedAround() {
        let selector = #selector(UIViewController.dismissKeyboard)
        let tapGesture = UITapGestureRecognizer(target: self, action: selector)
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc public func dismissKeyboard() {
        view.endEditing(true)
    }
}

