//
//  NSObject+ClassName.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/13/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

extension NSObject {
    
    var className: String {
        if let className = String(describing: self).components(separatedBy: ".").last {
            return className
        }
        return String(describing: self)
    }
    
    class var className: String {
        if let className = String(describing: self).components(separatedBy: ".").last {
            return className
        }
        return String(describing: self)
    }
}
