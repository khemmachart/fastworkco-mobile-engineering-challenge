//
//  ObjectMemoryObservableProtocol.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 24/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

public protocol ObjectMemoryObservableProtocol {
    
    func logDeinitObject()
    func getClassName() -> String
}

extension ObjectMemoryObservableProtocol {
    
    public func logInitObject() {
        #if DEBUG
            print("Init - " +  getClassName())
        #endif
    }
    
    public func logDeinitObject() {
        #if DEBUG
            print("Deinit - " +  getClassName())
        #endif
    }
    
    public func getClassName() -> String {
        return String(describing: type(of: self))
    }
}
