//
//  AppDelegate.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 11/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setRootViewController()
        
        return true
    }
    
    // MARK: - Utils
    
    private func setRootViewController() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
    }
    
    private var rootViewController: UIViewController {
        let navController = UINavigationController(rootViewController: ProductListRouter.createModule())
        navController.isNavigationBarHidden = true
        return navController
    }
}

