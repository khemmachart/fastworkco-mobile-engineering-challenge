//
//  ProductPhoto.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/11/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

struct ProductPhoto: Codable {
    
    let photoID: String?
    let productID: String?
    
    let imageMediumUrlString: String?
    let imageThumbnailUrlString: String?
    
    let isCoverPhoto: Bool?
    let sortOrder: Int?
    
    let createdDateString: String?
    let deletedDateString: String?
    let updatedDateString: String?
    
    enum CodingKeys: String, CodingKey {
        
        case photoID = "id"
        case productID = "product_id"
        
        case imageMediumUrlString = "image_medium"
        case imageThumbnailUrlString = "image_thumbnail"
    
        case isCoverPhoto = "is_cover_photo"
        case sortOrder = "sort_order"
        
        case createdDateString = "created_at"
        case deletedDateString = "deleted_at"
        case updatedDateString = "updated_at"
    }
}
