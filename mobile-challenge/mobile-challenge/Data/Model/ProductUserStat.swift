//
//  ProductUserStat.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/11/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

struct ProductUserStats: Codable {
    
    let statID: String?
    let userID: String?
    
    let completionRate: Int?
    let conversionRate: Int?
    let responseRate: Int?
    let responseTime: Double?
    
    enum CodingKeys: String, CodingKey {
        
        case statID = "id"
        case userID = "user_id"
        
        case completionRate = "completion_rate"
        case conversionRate = "conversion_rate"
        case responseRate = "response_rate"
        case responseTime = "response_time"
    }
}
