//
//  SortingCollectionItem.swift
//  mobile-challenge
//
//  Created by Khemmachart Chutapetch on 14/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

struct SortingCollectionItem {
    
    var title: String
}
