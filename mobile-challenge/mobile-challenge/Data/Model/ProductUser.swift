//
//  ProductUser.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/11/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

struct ProductUser: Codable {
    
    let userID: String?
    let username: String?
    let firstName: String?
    let imageUrlString: String?
    let lastOnlineDateString: String?
    let stat: ProductUserStats?
    
    enum CodingKeys: String, CodingKey {
        
        case userID = "id"
        case username = "username"
        case firstName = "first_name"
        case imageUrlString = "image"
        case lastOnlineDateString = "last_online_at"
        case stat
    }
}
