//
//  Product.swift
//  mobile-challenge
//
//  Created by KHUN NINE on 1/11/19.
//  Copyright © 2019 Khemmachart Chutapetch. All rights reserved.
//

import Foundation

struct Product: Codable {
    
    let productID: String?
    let title: String?
    let description: String?
    
    let purchase: Int?
    let rating: Int?
    let favorites: Int?
    
    let photos: [ProductPhoto]
    let user: ProductUser?
    
    var isUserFavorite: Bool = false
    
    enum CodingKeys: String, CodingKey {
        
        case productID = "objectID"
        case title
        case description
        
        case purchase
        case rating
        case favorites
        
        case photos
        case user
    }
}

extension Product: Equatable {
    
    static func == (lhs: Product, rhs: Product) -> Bool {
        return  lhs.productID == rhs.productID
    }
}
