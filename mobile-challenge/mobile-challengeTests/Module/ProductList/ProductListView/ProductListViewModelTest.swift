//
//  ProductListViewModelTest.swift
//  mobile-challengeTests
//
//  Created by Khemmachart Chutapetch on 20/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Nimble
import Quick

@testable import mobile_challenge

class ProductListViewModelTest: QuickSpec {
    
    override func spec() {
        
        var productListViewModel: ProductListViewModel?
        var tempProducts: [Product] = []
        
        let serviceManager = LocalServiceManager()
        
        describe("ProductListVerticalView") {
            
            beforeEach {
        
                let presenter = ProductListPresenter(interface: nil, interactor: nil, router: nil)
                presenter.favoriteProductIDs = ProductManagerMock().getFavoriteProductIDs()
                
                // Init product list view model
                waitUntil(timeout: 10, action: { done in
                    serviceManager.request(
                        dataRequest: ProductListServiceDataRequest(),
                        completion: { (result: Result<[Product]>) in
                            switch result {
                            case .success(let products):
                                presenter.products = products
                                productListViewModel = ProductListViewModel(products: presenter.getCombinedFavoriteProducts())
                                tempProducts = products
                            case .failure(_):
                                break
                            }
                            done()
                    })?.resume()
                })
            }
            
            context("when product is assigned") {
                
                it("should have 10 product") {
                    expect(productListViewModel?.getProducts().count).to(equal(10))
                }
                it("should generate product cover image urls") {
                    expect(productListViewModel?.getProductCoverImageUrls().count).to(equal(10))
                }
            }
            
            context("when favorite product IDs is assigned") {
                
                it("should have 4 favorite product IDs") {
                    expect(productListViewModel?.getProducts().filter({ $0.isUserFavorite }).count).to(equal(4))
                }
            }
            
            context("When get any product") {
                
                it("should return nil if product index is not exist") {
                    expect(productListViewModel?.getProduct(at: -1)?.productID).to(beNil())
                    expect(productListViewModel?.getProduct(at: 1000)?.productID).to(beNil())
                }
                it("should be as expected product ID") {
                    expect(productListViewModel?.getProduct(at: 0)?.productID).to(equal("dce7dc92-7bc4-4eba-ad9a-5d7f848a4605"))
                    expect(productListViewModel?.getProduct(at: 1)?.productID).to(equal("ebfeec86-e3a3-4e24-bf5f-6efba4beee51"))
                    expect(productListViewModel?.getProduct(at: 2)?.productID).to(equal("4675eddb-8e21-4676-ada3-52477d706cb7"))
                    expect(productListViewModel?.getProduct(at: 3)?.productID).to(equal("d7a67431-158d-43f5-bc0c-b802cc4e63e7"))
                    expect(productListViewModel?.getProduct(at: 4)?.productID).to(equal("14cc8afa-190c-41f3-a736-2adc75f0fd81"))
                    expect(productListViewModel?.getProduct(at: 5)?.productID).to(equal("df6f0185-5178-468a-bf46-6c93de1096a9"))
                    expect(productListViewModel?.getProduct(at: 6)?.productID).to(equal("21993fad-7a34-4e2f-aa98-e7df516b8585"))
                    expect(productListViewModel?.getProduct(at: 7)?.productID).to(equal("ed181f63-ec31-449b-848a-96744ec8d860"))
                    expect(productListViewModel?.getProduct(at: 8)?.productID).to(equal("446153e0-680e-4001-b132-66bfbef5c217"))
                    expect(productListViewModel?.getProduct(at: 9)?.productID).to(equal("0d58d781-43fb-46ac-8825-cc6e4fcf1c48"))
                    
                }
            }
            
            context("When get product cover url") {
                
                it("should return nil if product index is not exist") {
                    expect(productListViewModel?.getProductCoverImageUrl(at: -1)).to(beNil())
                    expect(productListViewModel?.getProductCoverImageUrl(at: 1000)).to(beNil())
                }
                it("should be as expected product cover url") {
                    expect(productListViewModel?.getProductCoverImageUrl(at: 0)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/1e25fff0-ece1-4aa7-8d23-2bbc09c29aa2.jpg"))
                    expect(productListViewModel?.getProductCoverImageUrl(at: 1)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/17683725-685d-40b2-b9f4-2f9534535e2d.jpg"))
                    expect(productListViewModel?.getProductCoverImageUrl(at: 2)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/c4120317-7494-4434-a974-2dfe8c93b014.jpg"))
                    expect(productListViewModel?.getProductCoverImageUrl(at: 3)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/35e1d240-aa28-4022-b696-546a08f5fb1a.jpg"))
                    expect(productListViewModel?.getProductCoverImageUrl(at: 4)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/58d26026-c6ba-41d4-b65f-1c3695d225d6.jpg"))
                    expect(productListViewModel?.getProductCoverImageUrl(at: 5)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/61e586d6-a1f6-473b-b043-13509a199ff3.jpg"))
                    expect(productListViewModel?.getProductCoverImageUrl(at: 6)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/cc66b275-6e78-4460-b3a9-31d366305df0.jpg"))
                    expect(productListViewModel?.getProductCoverImageUrl(at: 7)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/1aaddda4-d33e-4b69-90ed-fd1e82c614b4.jpg"))
                    expect(productListViewModel?.getProductCoverImageUrl(at: 8)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/06b0bb37-877f-4548-a95b-a41815061395.jpg"))
                    expect(productListViewModel?.getProductCoverImageUrl(at: 9)?.absoluteString).to(equal("https://storage.googleapis.com/fastwork-static/dad300de-b515-456e-b31e-e4895065cfb7.jpg"))
                }
            }
            
            context("When generate product cover photo images from product itself") {
                
                it("should be exactly the same number as number of cover photo") {
                    expect(productListViewModel?.getProductCoverImageUrls(from: tempProducts).count).to(equal(10))
                    expect(productListViewModel?.getProductCoverImageUrls(from: []).count).to(equal(0))
                }
                it("should should return empty array form empty product") {
                    expect(productListViewModel?.getProductCoverImageUrls(from: [])).to(equal([]))
                }
                it("should be correct url and be URL type") {
                    let firstCoverImageURL = URL(string: tempProducts.first?.photos.first?.imageThumbnailUrlString ?? "")!
                    let lastCoverImageURL = URL(string: tempProducts.last?.photos.first?.imageThumbnailUrlString ?? "")!
                    expect(productListViewModel?.getProductCoverImageUrls(from: tempProducts).first).to(equal(firstCoverImageURL))
                    expect(productListViewModel?.getProductCoverImageUrls(from: tempProducts).last).to(equal(lastCoverImageURL))
                }
            }
            
            context("When getting a product cover image url string list") {
                
                it("should be exactly the same number as number of cover photo string") {
                    expect(productListViewModel?.getProductCoverImageUrlStringList(from: tempProducts).count).to(equal(10))
                    expect(productListViewModel?.getProductCoverImageUrlStringList(from: []).count).to(equal(0))
                }
                it("should should return empty array form empty product") {
                    expect(productListViewModel?.getProductCoverImageUrlStringList(from: [])).to(equal([]))
                }
                it("should be correct url and be String type") {
                    let firstCoverImageURLString = tempProducts.first?.photos.first?.imageThumbnailUrlString
                    let lastCoverImageURLString = tempProducts.last?.photos.first?.imageThumbnailUrlString
                    expect(productListViewModel?.getProductCoverImageUrlStringList(from: tempProducts).first).to(equal(firstCoverImageURLString))
                    expect(productListViewModel?.getProductCoverImageUrlStringList(from: tempProducts).last).to(equal(lastCoverImageURLString))
                }
            }
            
            context("When getting product cover image url string") {
                
                it("should be return a right cover image for each product") {
                    let firstProduct = tempProducts[0]
                    expect(productListViewModel?.getProductCoverImageUrlString(from: firstProduct)).to(equal(firstProduct.photos[0].imageThumbnailUrlString))
                    expect(productListViewModel?.getProductCoverImageUrlString(from: firstProduct)).toNot(equal(firstProduct.photos[1].imageThumbnailUrlString))
                    expect(productListViewModel?.getProductCoverImageUrlString(from: firstProduct)).toNot(equal(firstProduct.photos[2].imageThumbnailUrlString))
                    expect(productListViewModel?.getProductCoverImageUrlString(from: firstProduct)).toNot(equal(firstProduct.photos[3].imageThumbnailUrlString))
                }
            }
            
            context("When getting product view model") {
                
                it("should be return a right product view model") {
                    expect(productListViewModel?.getProductTableViewCellViewModel(at: 0).getProduct())
                        .to(equal(productListViewModel?.getProduct(at: 0)))
                    expect(productListViewModel?.getProductTableViewCellViewModel(at: 1).getProduct())
                        .to(equal(productListViewModel?.getProduct(at: 1)))
                    expect(productListViewModel?.getProductTableViewCellViewModel(at: 2).getProduct())
                        .to(equal(productListViewModel?.getProduct(at: 2)))
                    expect(productListViewModel?.getProductTableViewCellViewModel(at: 2).getProduct())
                        .to(equal(productListViewModel?.getProduct(at: 2)))
                    expect(productListViewModel?.getProductTableViewCellViewModel(at: 3).getProduct())
                        .to(equal(productListViewModel?.getProduct(at: 3)))
                }
            }
        }
    }
}

