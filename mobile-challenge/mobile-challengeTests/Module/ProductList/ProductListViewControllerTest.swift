//
//  ProductListViewControllerTest.swift
//  mobile-challengeTests
//
//  Created by Khemmachart Chutapetch on 17/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Nimble
import Quick

@testable import mobile_challenge

class ProductListViewControllerTest: QuickSpec {
    
    override func spec() {
        
        var view: ProductListViewController?
        var interactor: ProductListInteractor?
        
        describe("ProductListPresenter") {
            
            beforeEach {
                let productMgr = ProductManagerMock()
                let localServiceMgr = LocalServiceManager()
                
                interactor = ProductListInteractor(serviceManager: localServiceMgr, productManager: productMgr)
                view = ProductListViewController()
                
                let presenter = ProductListPresenter(interface: view, interactor: nil, router: nil)
                presenter.favoriteProductIDs = productMgr.getFavoriteProductIDs()
                
                view?.productItemListView = ProductListVerticalView()
                view?.presenter = presenter
                
                waitUntil(timeout: 10, action: { done in
                    localServiceMgr.request(
                        dataRequest: ProductListServiceDataRequest(),
                        completion: { (result: Result<[Product]>) in
                            switch result {
                            case .success(let products):
                                presenter.products = products
                                view?.productItemListView?.viewModel = ProductListViewModel(products: presenter.getCombinedFavoriteProducts())
                            case .failure(_):
                                break
                            }
                            done()
                    })?.resume()
                })
            }
            
            context("when request products") {
                
                it("Product item list view should have 4 favorite product IDs") {
                    expect(view?.productItemListView?.viewModel?.getProducts().filter({ $0.isUserFavorite }).count).to(equal(4))
                }
            }
            
            context("when request products") {
                
                it("Product item list view should have 10 products") {
                    waitUntil(timeout: 10, action: { done in
                        LocalServiceManager().request(
                            dataRequest: ProductListServiceDataRequest(),
                            completion: { (result: Result<[Product]>) in
                                switch result {
                                case .success(let products):
                                    interactor?.presenter?.requestProductListServiceDidSuccess(products: products)
                                case .failure(let error):
                                    interactor?.presenter?.requestProductListServiceDidFail(errorMessage: error.localizedDescription)
                                }
                                expect(view?.productItemListView?.viewModel?.getProducts().count).to(equal(10))
                                done()
                        })?.resume()
                    })
                }
                
                it("Product cover image list should be generated from products") {
                    waitUntil(timeout: 10, action: { done in
                        LocalServiceManager().request(
                            dataRequest: ProductListServiceDataRequest(),
                            completion: { (result: Result<[Product]>) in
                                switch result {
                                case .success(let products):
                                    interactor?.presenter?.requestProductListServiceDidSuccess(products: products)
                                case .failure(let error):
                                    interactor?.presenter?.requestProductListServiceDidFail(errorMessage: error.localizedDescription)
                                }
                                expect((view?.productItemListView as? ProductListVerticalView)?.viewModel?.getProductCoverImageUrls().count).to(equal(10))
                                done()
                        })?.resume()
                    })
                }
            }
        }
    }
}
