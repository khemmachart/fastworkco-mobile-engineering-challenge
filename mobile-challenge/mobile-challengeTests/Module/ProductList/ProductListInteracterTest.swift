//
//  ProductListInteracterTest.swift
//  mobile-challengeTests
//
//  Created by Khemmachart Chutapetch on 17/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Nimble
import Quick

@testable import mobile_challenge

class ProductListInteracterText: QuickSpec {
    
    override func spec() {
        
        var interactor: ProductListInteractor?
        
        describe("ProductListInteracter") {
            
            beforeEach {
                let productMgr = ProductManagerMock()
                let localServiceMgr = LocalServiceManager()
                
                interactor = ProductListInteractor(serviceManager: localServiceMgr, productManager: productMgr)
                interactor?.requestFavoriteProductIDs()
            }
            
            context("when request products") {
                
                it("should have 4 favorite product IDs") {
                    expect(interactor?.productManager?.getFavoriteProductIDs().count).to(equal(4))
                }
                
                it("should have 4 correct favorite product ID") {
                    expect(interactor?.productManager?.getFavoriteProductIDs()[0]).to(equal("dce7dc92-7bc4-4eba-ad9a-5d7f848a4605"))
                    expect(interactor?.productManager?.getFavoriteProductIDs()[1]).to(equal("ebfeec86-e3a3-4e24-bf5f-6efba4beee51"))
                    expect(interactor?.productManager?.getFavoriteProductIDs()[2]).to(equal("21993fad-7a34-4e2f-aa98-e7df516b8585"))
                    expect(interactor?.productManager?.getFavoriteProductIDs()[3]).to(equal("0d58d781-43fb-46ac-8825-cc6e4fcf1c48"))
                }
            }
        }
    }
}
