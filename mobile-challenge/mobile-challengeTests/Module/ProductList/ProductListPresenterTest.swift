//
//  mobile_challengeTests.swift
//  mobile-challengeTests
//
//  Created by Khemmachart Chutapetch on 11/1/2562 BE.
//  Copyright © 2562 Khemmachart Chutapetch. All rights reserved.
//

import Nimble
import Quick

@testable import mobile_challenge

class ProductListPresenterText: QuickSpec {
    
    override func spec() {
        
        var presenter: ProductListPresenter?
        
        describe("ProductListViewController") {
            
            beforeEach {
                let productMgr = ProductManagerMock()
                let localServiceMgr = LocalServiceManager()
            
                presenter = ProductListPresenter(interface: nil, interactor: nil, router: nil)
                presenter?.favoriteProductIDs = productMgr.getFavoriteProductIDs()
                
                waitUntil(timeout: 10, action: { done in
                    localServiceMgr.request(
                        dataRequest: ProductListServiceDataRequest(),
                        completion: { (result: Result<[Product]>) in
                            switch result {
                            case .success(let products):
                                presenter?.products = products
                            case .failure(_):
                                break
                            }
                            done()
                    })?.resume()
                })
            }
            
            context("when request products") {

                it("should have 4 favorite product IDs") {
                    expect(presenter?.favoriteProductIDs.count).to(equal(4))
                }
                
                it("should have 4 correct favorite product ID") {
                    expect(presenter?.favoriteProductIDs[0]).to(equal("dce7dc92-7bc4-4eba-ad9a-5d7f848a4605"))
                    expect(presenter?.favoriteProductIDs[1]).to(equal("ebfeec86-e3a3-4e24-bf5f-6efba4beee51"))
                    expect(presenter?.favoriteProductIDs[2]).to(equal("21993fad-7a34-4e2f-aa98-e7df516b8585"))
                    expect(presenter?.favoriteProductIDs[3]).to(equal("0d58d781-43fb-46ac-8825-cc6e4fcf1c48"))
                }
                
                
                it("should return correct number of favorite products") {
                    expect(presenter?.getCombinedFavoriteProducts().filter({ $0.isUserFavorite }).count).to(equal(4))
                }
                
                it("should return correct combined favorite products") {
                    expect(presenter?.getCombinedFavoriteProducts().filter({ $0.isUserFavorite })[0].productID)
                        .to(equal("dce7dc92-7bc4-4eba-ad9a-5d7f848a4605"))
                    expect(presenter?.getCombinedFavoriteProducts().filter({ $0.isUserFavorite })[1].productID)
                        .to(equal("ebfeec86-e3a3-4e24-bf5f-6efba4beee51"))
                    expect(presenter?.getCombinedFavoriteProducts().filter({ $0.isUserFavorite })[2].productID)
                        .to(equal("21993fad-7a34-4e2f-aa98-e7df516b8585"))
                    expect(presenter?.getCombinedFavoriteProducts().filter({ $0.isUserFavorite })[3].productID)
                        .to(equal("0d58d781-43fb-46ac-8825-cc6e4fcf1c48"))
                }
            }
        }
    }
}
